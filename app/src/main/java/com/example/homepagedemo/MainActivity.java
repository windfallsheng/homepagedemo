package com.example.homepagedemo;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;

import com.example.homepagedemo.dummy.DummyContent;
import com.example.homepagedemo.sticky.StickyItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ItemFragment.OnListFragmentInteractionListener {

    private CommodityListAdapter mCommodityListAdapter;
    private List<CommodityInfo> mCommodityInfoList;
    private OutRecyclerView mRecyclerView;
    private LinearLayoutManager mLLayoutManager;

    int lastDy = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recyclerview);
        mLLayoutManager = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setLayoutManager(mLLayoutManager);
//        mRecyclerView.addItemDecoration(new StickyItemDecoration());
        mRecyclerView.addItemDecoration(new RecyclerViewDivider(MainActivity.this, LinearLayoutManager.HORIZONTAL, 20, ContextCompat.getColor(MainActivity.this, R.color.btn_gray_f2)));
        List<CommodityInfo> commodityList = buildComondityList();
        List<CommodityCategory> CategoryList = buildCommodityCategoryList(1);
        HomeCommodity h1 = new HomeCommodity(1, commodityList, CategoryList);
        HomeCommodity h2 = new HomeCommodity(1, commodityList, CategoryList);
        HomeCommodity h3 = new HomeCommodity(2, commodityList, CategoryList);
        HomeCommodity h4 = new HomeCommodity(2, commodityList, CategoryList);
//        HomeCommodity h5 = new HomeCommodity(3, null, null);
        HomeCommodity h6 = new HomeCommodity(4, null, null);

        final List<HomeCommodity> homeCommodityList = new ArrayList<>();
        homeCommodityList.add(h1);
        homeCommodityList.add(h2);
        homeCommodityList.add(h3);
        homeCommodityList.add(h4);
//        homeCommodityList.add(h5);
        homeCommodityList.add(h6);
        mCommodityListAdapter = new CommodityListAdapter(MainActivity.this);
        mCommodityListAdapter.setHomeCommodityList(homeCommodityList);
        mRecyclerView.setAdapter(mCommodityListAdapter);


        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(MainActivity.this) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
//        smoothScroller.setTargetPosition(4);
//        mLLayoutManager.startSmoothScroll(smoothScroller);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int firstVisibleItemPosition = mLLayoutManager.findFirstVisibleItemPosition();
                if (dy - lastDy > 0 && homeCommodityList.get(firstVisibleItemPosition).type == 4) {// 向上滑动
                    mRecyclerView.setNeedIntercept(false);
                }
                lastDy = dy;
                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }

    public void adjustIntercept(boolean b) {
        mRecyclerView.setNeedIntercept(b);
    }


    private CommodityInfo buildComondityInfo() {
        CommodityInfo commodityInfo = new CommodityInfo("商品名", "商品描述", "");
        return commodityInfo;
    }

    private List<CommodityInfo> buildComondityList() {
        List<CommodityInfo> commodityList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            CommodityInfo commodityInfo = buildComondityInfo();
            commodityList.add(commodityInfo);
        }
        return commodityList;
    }

    private CommodityCategory buildCommodityCategory(int category) {
        CommodityCategory commodityCategory = new CommodityCategory(category, "分类名", "");
        return commodityCategory;
    }

    private List<CommodityCategory> buildCommodityCategoryList(int category) {
        List<CommodityCategory> commodityCategoryList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            CommodityInfo commodityInfo = buildComondityInfo();
            CommodityCategory commodityCategory = buildCommodityCategory(category);
            commodityCategoryList.add(commodityCategory);
        }
        return commodityCategoryList;
    }


    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }
}
