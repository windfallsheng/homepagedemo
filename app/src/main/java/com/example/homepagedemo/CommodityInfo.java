package com.example.homepagedemo;

public class CommodityInfo {


    public String name;
    public String desc;
    public String iconUrl;

    public CommodityInfo(String name, String desc, String iconUrl) {
        this.name = name;
        this.desc = desc;
        this.iconUrl = iconUrl;
    }
}
