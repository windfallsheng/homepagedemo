package com.example.homepagedemo;

public class CommodityCategory {

    public int classification;
    public String name;
    public String iconUrl;

    public CommodityCategory(int classification, String name, String iconUrl) {
        this.classification = classification;
        this.name = name;
        this.iconUrl = iconUrl;
    }
}
