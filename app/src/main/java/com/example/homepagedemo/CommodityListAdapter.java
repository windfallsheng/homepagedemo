package com.example.homepagedemo;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyco.tablayout.SlidingTabLayout;
import com.nshmura.recyclertablayout.RecyclerTabLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @CreateDate: 2018/1/26
 * @Author: lzsheng
 * @Description: 适配器，根据不同的数据类型，展示不同的UI效果
 * @Version:
 */
public class CommodityListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<HomeCommodity> mHomeCommodityList;
    int height;

    public CommodityListAdapter(Context context) {
        this.mContext = context;
        //状态栏高度
        int statusBarHeight = 0;
        int resourceId = mContext.getResources().getIdentifier("status_bar_height", "dimen",
                "android");
        if (resourceId > 0) {
            statusBarHeight = mContext.getResources().getDimensionPixelSize(resourceId);
        }
        //屏幕高度
        DisplayMetrics dm = mContext.getApplicationContext().getResources().getDisplayMetrics();
        final float scale = dm.density;
        int i = (int) (54 * scale + 0.5f);
        height = dm.heightPixels - statusBarHeight - i;
    }

    public void setHomeCommodityList(List<HomeCommodity> homeCommodityList) {
        mHomeCommodityList = homeCommodityList;
    }

    @Override
    public int getItemViewType(int position) {
        if (mHomeCommodityList != null && mHomeCommodityList.size() > 0) {
            HomeCommodity homeCommodity = mHomeCommodityList.get(position);
            int type = homeCommodity.type;
            return type;
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new UnknowViewHolder(
                        LayoutInflater.from(mContext).inflate(R.layout.rv_item_commodity_unknow, parent, false));
            case 1:
                return new CategoryViewHolder(
                        LayoutInflater.from(mContext).inflate(R.layout.rv_item_commodity_category, parent, false));
            case 2:
                return new CommodityViewHolder(
                        LayoutInflater.from(mContext).inflate(R.layout.rv_item_commodity_info, parent, false));
            case 3:
                return new CategoryNevViewHolder(
                        LayoutInflater.from(mContext).inflate(R.layout.rv_item_commodity_category_navigation, parent, false));
            case 4:
                return new CategoryVpViewHolder(
                        LayoutInflater.from(mContext).inflate(R.layout.rv_item_commodity_category_vp, parent, false));
            default:
                return new UnknowViewHolder(
                        LayoutInflater.from(mContext).inflate(R.layout.rv_item_commodity_unknow, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        int viewType = getItemViewType(position);
        HomeCommodity homeCommodity = mHomeCommodityList.get(position);
        int type = homeCommodity.type;
        switch (type) {
            case 0: {
                UnknowViewHolder unknowHolder = (UnknowViewHolder) holder;

            }
            break;
            case 1: {
                CategoryViewHolder categoryHolder = (CategoryViewHolder) holder;

            }
            break;
            case 2: {
                CommodityViewHolder commodityHolder = (CommodityViewHolder) holder;

            }
            break;
            case 3: {
                CategoryNevViewHolder categoryNevHolder = (CategoryNevViewHolder) holder;
                categoryNevHolder.tvName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mContext, "分类被点击", Toast.LENGTH_SHORT).show();
                    }
                });

//                List<ColorItem> items = DemoData.loadDemoColorItems(mContext);
//                Collections.sort(items, new Comparator<ColorItem>() {
//                    @Override
//                    public int compare(ColorItem lhs, ColorItem rhs) {
//                        return lhs.name.compareTo(rhs.name);
//                    }
//                });
//
//                DemoColorPagerAdapter adapter = new DemoColorPagerAdapter();
//                adapter.addAll(items);
//                categoryNevHolder.vp.setAdapter(adapter);
//                categoryNevHolder.tabLayout.setUpWithViewPager(categoryNevHolder.vp);
            }
            break;
            case 4: {
                final String[] mTitles = {"热门", "iOS", "Android", "前端", "PHP", "JavaScript",};
                CategoryVpViewHolder categoryVpHolder = (CategoryVpViewHolder) holder;
//                sl_root = (ScrollableLayout) findViewById(R.id.sl_root);
//                listview = (ListView) findViewById(R.id.vp_scroll);
//                sl_root.getHelper().setCurrentScrollableContainer(listview);

                List<Fragment> fragmentList = new ArrayList<>();
                for (int i = 0; i < mTitles.length; i++) {
                    ItemFragment item = ItemFragment.newInstance(2);
                    fragmentList.add(item);
                }
                NevFragmentAdapter adapter = new NevFragmentAdapter(((MainActivity) mContext).getSupportFragmentManager(), fragmentList);
                categoryVpHolder.vp.setAdapter(adapter);
                categoryVpHolder.vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i1) {

                    }

                    @Override
                    public void onPageSelected(int i) {
                        Toast.makeText(mContext, "第 " + (i + 1) + " 个item", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {

                    }
                });
                categoryVpHolder.tab.setViewPager(categoryVpHolder.vp, mTitles);
                //RecyclerView嵌套ViewPager会出现高度为0的bug,这里给ViewPager设置的高度为屏幕高度-状态栏高度
                ViewGroup.LayoutParams layoutParams = categoryVpHolder.itemView.getLayoutParams();
                layoutParams.height = height;
                Log.d("sheng", "CommodityListAdapter#categoryVpHolder.itemView.setLayoutParams#layoutParams");
                categoryVpHolder.itemView.setLayoutParams(layoutParams);
            }
            break;
            default:
                UnknowViewHolder unknowHolder = (UnknowViewHolder) holder;
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (mHomeCommodityList != null && mHomeCommodityList.size() > 0) {
            return mHomeCommodityList.size();
        }
        return 0;
    }

    class UnknowViewHolder extends RecyclerView.ViewHolder {
        ImageView ivIcon;
        TextView tvName;
        TextView tvDesc;

        UnknowViewHolder(View itemView) {
            super(itemView);
            itemView.setTag(false);
//            ivIcon = (ImageView) itemView.findViewById(R.id.imageview_head_icon);
//            tvName = (TextView) itemView.findViewById(R.id.textview_chat_name);
//            tvDesc = (TextView) itemView.findViewById(R.id.textview_last_msg_content);
        }
    }

    class CommodityViewHolder extends RecyclerView.ViewHolder {
        ImageView ivIcon;
        TextView tvName;
        TextView tvDesc;

        CommodityViewHolder(View itemView) {
            super(itemView);
            itemView.setTag(false);
//            ivIcon = (ImageView) itemView.findViewById(R.id.imageview_head_icon);
//            tvName = (TextView) itemView.findViewById(R.id.textview_chat_name);
//            tvDesc = (TextView) itemView.findViewById(R.id.textview_last_msg_content);
        }
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView ivIcon;
        TextView tvName;
        TextView tvDesc;

        CategoryViewHolder(View itemView) {
            super(itemView);
            itemView.setTag(false);
//            ivIcon = (ImageView) itemView.findViewById(R.id.imageview_head_icon);
//            tvName = (TextView) itemView.findViewById(R.id.textview_chat_name);
//            tvDesc = (TextView) itemView.findViewById(R.id.textview_last_msg_content);
        }
    }

    class CategoryNevViewHolder extends RecyclerView.ViewHolder {
        ViewPager vp;
        RecyclerTabLayout tabLayout;
        ImageView ivIcon;
        TextView tvName;
        TextView tvDesc;

        CategoryNevViewHolder(View itemView) {
            super(itemView);
            itemView.setTag(false);
//            vp = (ViewPager) itemView.findViewById(R.id.view_pager);
//            tabLayout = (RecyclerTabLayout) itemView.findViewById(R.id.recycler_tab_layout);

//            ivIcon = (ImageView) itemView.findViewById(R.id.imageview_head_icon);
            tvName = (TextView) itemView.findViewById(R.id.textview_name);
//            tvDesc = (TextView) itemView.findViewById(R.id.textview_last_msg_content);
        }
    }

    class CategoryVpViewHolder extends RecyclerView.ViewHolder {
        ViewPager vp;
        TextView tvName;
        SlidingTabLayout tab;
        TextView tvDesc;

        CategoryVpViewHolder(View itemView) {
            super(itemView);
            itemView.setTag(true);
            vp = (ViewPager) itemView.findViewById(R.id.vp_category_nev);
            tab = (SlidingTabLayout) itemView.findViewById(R.id.tl_4);
//            ivIcon = (ImageView) itemView.findViewById(R.id.imageview_head_icon);
//            tvName = (TextView) itemView.findViewById(R.id.textview_chat_name);
//            tvDesc = (TextView) itemView.findViewById(R.id.textview_last_msg_content);
        }
    }

}
