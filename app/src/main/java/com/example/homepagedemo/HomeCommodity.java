package com.example.homepagedemo;

import java.util.List;

public class HomeCommodity {


    public int type;
    public List<CommodityInfo> commodityList;
    public List<CommodityCategory> commodityCategoryList;

    public HomeCommodity(int type, List<CommodityInfo> commodityList, List<CommodityCategory> commodityCategoryList) {
        this.type = type;
        this.commodityList = commodityList;
        this.commodityCategoryList = commodityCategoryList;
    }
}
